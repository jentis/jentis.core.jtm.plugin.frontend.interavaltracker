(!code:start)
function(jtsTracker,oGateConfig){

	this.oConf = oGateConfig;
	this.oTracker = jtsTracker;
	
	this.init = function(){
		this.oTracker.registerGateFunc("pageview",this.pageview,this);
	}
	
	this.pageview = function(){
		clearInterval(this.fnInterval)

		var iCount = parseInt(this.oConf.limit);

		this.fnInterval = setInterval(function() {
			
			window._jts.push({
				"track" : "event",
				"group" : this.oConf.event_group,
				"name" 	: this.oConf.event_name,
				"value" : this.oConf.event_value,
			});

			iCount--;

			if (iCount === 0) {
				clearInterval(this.fnInterval)
			}
		}.bind(this), parseInt(this.oConf.timer));
	}
	
	this.init();
}
(!code:end)